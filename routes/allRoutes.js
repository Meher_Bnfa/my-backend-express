const express = require("express");
const router = express.Router();
const User = require("../models/customerSchema");
var moment = require("moment");
const userController = require("../controller/userController");
const AuthUser = require("../models/authUser");
const bcrypt = require("bcrypt");
var jwt = require("jsonwebtoken");
var { requireAuth } = require("../middleware/middleware");
const { checkIfUser } = require("../middleware/middleware");
const { check, validationResult } = require("express-validator");
const authController = require("../controller/authController");

router.get("*", checkIfUser);

// Level 2

// Level 2

router.get("/signout", authController.get_signout);

router.get("/login", authController.get_login);

router.get("/signup", authController.get_signup);

router.post(
  "/signup",
  [
    check("email", "Please provide a valid email").isEmail(),
    check(
      "password",
      "Password must be at least 8 characters with 1 upper case letter and 1 number"
    ).matches(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/),
  ],
  authController.post_signup
);

router.post("/login", authController.post_login);

router.get("/", authController.get_welcome);

// Level 1
//Get request
router.get("/home", requireAuth, userController.user_index_get);
router.get("/user/add.html", (req, res) => {
  res.render("user/add");
});
router.get("/edit/:id", requireAuth, userController.user_edit_get);
router.get("/user/:id", requireAuth, userController.user_view_get);
//Post request
router.post("/search", userController.user_search_post);
//delete request
router.delete("/edit/:id", userController.user_delete);
// PUT request
router.put("/edit/:id", userController.user_put);

module.exports = router;
